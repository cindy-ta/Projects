/*
 * Lab 3
 * Embedded Systems and Systems Software
 * 
 *  Created: 2/21/2015 1:57:26 PM
 * 
 *  Cindy Ta
 *  Phil Partipilo
 */ 


 .include "tn45def.inc"

 ; define and initialize variables used to find direction of turning 
 .def a = r17
 .def b = r18
 .def c = r19
 .def d = r20 

 ; define timer variables
 .def tmp1 = r23		; Use r23 for temporary variables
 .def tmp2 = r24		; Use r24 for temporary values
 .def count = r25		; Use r25 for reloading timer

 ; define global variable for duty cycle
  .def dutycycle = r21

 ; Start code segment
 .cseg
 .org 0

 ; initialize global variables
 ldi dutycycle, 102 ; sets duty cycle to 50%
 ldi a, 1
 ldi b, 1
 ldi c,1
 ldi d, 1

 ; Configure TIMER0 for input from system clock
 ; prescaled by 8. At 10 MHz, this will increment
 ; the clock at 1.250 MHz
	ldi tmp1,0x02
	out TCCR0B, tmp1

 ; Configure ports for output
	sbi DDRB,2		; PB2 is now output, input pulses

 ; Configure pins for input
 	cbi DDRB,0		; PB0 is an input - Channel A
	cbi DDRB,1		; PB1 is an input - Channel B

 ; Main loop:
 	main:
		rcall getRPGDir			; Get direction of RPG
		ldi count, 205			; acquires 3.95kHz frequency
		cbi PORTB,2				; pull PB2 low
		sub count, dutycycle	; subtracts dutycycle from count, stores back into count
		rcall delay				; times delay of PB2 low

        sbi PORTB,2			    ; pull PB2 high
		mov count, dutycycle	; swaps dutycycle value with count value
		rcall delay				; times delay of PB2 high

        rjmp main				; loop


; getRPGDir: Gets the direction of RPG. Clockwise or counterclockwise
	getRPGDir:
		mov c, a ; moves a into c
		mov d, b ; moves b into d

		; reinitialize a and b registers to bit 0
		ldi a, 0
		ldi b, 0

		; Checks signal in Channel A
		sbis PINB,0		; if 0 then skip
		ldi a, 1		; if 1 then load 1 into a

		; Checks signal in Channel B
		sbis PINB,1		; if 0 then skip
		ldi b, 1		; if 1 then load 1 into b
		
		; Compares for clockwise rotation
		cpse a, d
		rcall incDutyCycle

		; Compares for counter-clockwise rotation
		cpse b, c
		rcall decDutyCycle
		
		; If neither compares work then no rotation (11)

		end:
			ret

; Increment duty cycle - clockwise
; 40 bits reflects 70%
; Loops back up to main if duty cycle is at high point
; Otherwise decrement duty cycle
	incDutyCycle:
		cpi dutycycle, 40
		breq end
		dec dutycycle
		ret

; Decrement duty cycle - counter-clockwise
; 160 bits reflects 30%
; Loops back up to main if duty cycle is at low point
; Otherwise increments duty cycle
	decDutyCycle:
		cpi dutycycle, 160
		breq end
		inc dutycycle
		ret

; Wait for TIMER0 to roll over, prescaled by 8
delay:
		in tmp1, TCCR0B	; Save configuration
		ldi tmp2, 0x00	; Stop timer with 0
		out TCCR0B, tmp2 ; outputs timer stop

; Clear over flow flag
		in tmp2, TIFR		; tmp <-- TIFR
		sbr tmp2, 1<<TOV0	; Clear TOV0, write logic 1
		out TIFR, tmp2

; Start timer with new initial count
		out TCNT0, count	; load counter, read/write operations
		out TCCR0B, tmp1	; Restart timer

wait:
		in tmp2, TIFR		; tmp <-- TIFR
		sbrs tmp2, TOV0		; check overflow flag
		rjmp wait
		ret

.exit