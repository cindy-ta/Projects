/*
 * AssemblerApplication4.asm
 *
 *  Created: 2/25/2015 1:41:39 PM
 *
 *   Cindy Ta
 *   Phil Partipilo
 *
 */

 .include "m88padef.inc"

 .def temp = r1
 .def newA = r2
 .def newB = r3
 .def oldA = r4
 .def oldB = r5
 
 .def delayVar = r6
 .def delayVar1 = r7

 .def savingTemp = r8

 .def frequency = r9

 .def dutycycle = r10
 
 .def TCNT1 = r11

 .def countHigh = r12
 .def countLow = r13

 ; Isolating digits of the duty cycle
 .def drem16uL = r14
 .def drem16uH = r15
 .def dres16uL = r16
 .def dres16uH = r17
 .def dd16uL = r16        ; Dividend (where duty cycle goes)
 .def dd16uH = r17        ; Dividend
 .def dv16uL = r18        ; Divisor (10)
 .def dv16uH = r19        ; Divisor
 .def dcnt16u = r20

 .def tmp1 = r21
 .def tmp2 = r22


 .def onLow = r24
 .def onHigh = r25
 .def offLow = r26
 .def offHigh = r27
 .def dcLow = r28    ; duty cycle low
 .def dcHigh = r29
 
 .dseg
    ; Create 5 bytes in RAM to hold string.
    dtxt: .BYTE 5    ; Allocation
   

 .cseg
 
 .org 0x00                ; PC points here after power up,
    rjmp reset            ; hareware reset, WDT timeout

.org 0x001                ; Interrupt for button switch
    rjmp getFrequency    ; Frequency change
    reti

.org 0x005                ; Interrupt for RPG
    rjmp getRPGDir        ; Duty cycle change

 .org 0x00D                ; PC points here on timer/counter 1 Overflow
    rjmp tim1_ovf        ; over flow intterupt

 .org 0x1a                ; Past the last ISR vector

 msg1: .DB "Freq = ", 0x00
 freq1: .DB "1", 0x00
 freq2: .DB "2" , 0x00
 freq3: .DB "4", 0x00
 msg2: .DB " kHz", 0x00
 msg3: .DB "DC = ", 0x00
 msg4: .DB " %", 0x00

 reset:

    ; Configure pins and ports
    sbi DDRB, 2        ; PB2 is output
    sbi DDRB, 3        ; PB3 is output (LCD Enable)
    sbi DDRB, 5        ; PB5 is output (Command mode, RS)
    cbi DDRD, 0        ; PD0 is input (RPG -> A)
    cbi DDRD, 1        ; PD1 is input (RPG -> B)
    cbi DDRD, 2        ; PD2 is input (button)

    sbi PORTB, 2

    ; Interrupt for button
    lds tmp1, EIMSK
    ori tmp1, 1<<0
    out EIMSK, tmp1            ;enable int0
       
    lds tmp1, EICRA
    ori tmp1, 0x02            ; only for falling edge
    sts EICRA, tmp1

    lds tmp1, 0x68
    ori tmp1, 1<<2
    sts 0x68, tmp1

    lds tmp1, PCMSK2
    ori tmp1, 1<<0|1<<1        ; enable int 16 and 17
    sts PCMSK2, tmp1


    ; Configuring global variables
    ldi tmp1,1;
    mov frequency,tmp1
    ldi onHigh, 0
    ldi onLow, 0
    ldi dcHigh, HIGH(500)
    ldi dcLow, LOW(500)

    ; Configuring timers
    ldi tmp1,0x04
    sts TCCR1B, tmp1
    ldi tmp1,0

    ; Configure lower nibble of PORTC as output
    ldi r23, 0x0F
    out DDRC, r23

    ; Configure PB3 and PB5 output
    sbi PORTB,3
    sbi PORTB,5

    ; Initialize LCD
    rcall LCDInit
    ldi tmp1,3;

    mov frequency,tmp1;
    rcall LCD
loop:
    sei            ; Enable interrupts
        rjmp loop
main:
    sei            ; Enable interrupts


    rjmp LCD
    rjmp main

LCD:
    ; Sets cursor to beginning
    cbi PORTB, 5
    ldi r23, 0x80
    swap r23
    out PORTC, r23
    rcall LCDStrobe

    rcall delay_200us
    swap r23
    out PORTC, r23
    rcall LCDStrobe
    rcall delay_200us

    sbi PORTB, 5

    ; Displays Freq =
    ldi r21, 7
    ldi r30, LOW(2*msg1)
    ldi r31, HIGH(2*msg1)
    rcall displayCString
   
    ; Prints frequency
    rcall printFreq

    ; Displays Hz
    ldi r21, 4
    ldi r30, LOW(2*msg2)
    ldi r31, HIGH(2*msg2)
    rcall displayCString

    ; NEEDS TO MOVE CURSOR TO NEXT LINE
    cbi PORTB, 5
    ldi r23, 0xC0
    swap r23
    out PORTC, r23
    rcall LCDStrobe

    rcall delay_200us
    swap r23
    out PORTC, r23
    rcall LCDStrobe

    rcall delay_200us

    sbi PORTB, 5

    ; Display string: "DC = "
    ldi r21, 5
    ldi r30, LOW(2*msg3)    ; Load z register low
    ldi r31, HIGH(2*msg3)    ; Load z register high
    rcall displayCString

    ; Displays duty cycle
    ;rcall displayDC

    ; Display string: " %"
    ldi r21, 2
    ldi r30, LOW(2*msg4)    ; Load z register low
    ldi r31, HIGH(2*msg4)    ; Load z register high
    rcall displayCString

    cbi PORTB, 5

    ret

; ========================================
; Displaying frequencies
; ========================================
printFreq:

    ; figures out what kHz is cycling on
    ldi tmp1,1;
    cp frequency, tmp1
    breq freq_1

    ldi tmp1,2
    cp frequency, tmp1
    breq freq_2

    ldi tmp1,3
    cp frequency, tmp1
    breq freq_3

    ldi tmp1,4
    cp frequency, tmp1
    ldi tmp1,1
    mov frequency, tmp1    ; reset frequency to 1
    breq freq_1

freq_1:
    ldi r21, 1
    ldi r30, LOW(2*freq1)
    ldi r31, HIGH(2*freq1)
    rcall displayCString
    rjmp stop

freq_2:
    ldi r21, 1
    ldi r30, LOW(2*freq2)
    ldi r31, HIGH(2*freq2)
    rcall displayCString
    rjmp stop

freq_3:
    ldi r21, 1
    ldi r30, LOW(2*freq3)
    ldi r31, HIGH(2*freq3)
    rcall displayCString
    rjmp stop

stop:
    ret

; ========================================
; Delay:
; ========================================
delay:
    ; Wait for TIMER0 to roll over, prescaled by 8
    lds tmp1, TCCR1B    ; Save configuration
    ldi tmp2, 0x00        ; Stop timer with 0
    sts TCCR1B, tmp2    ; outputs timer stop
      
    ; Clear over flow flag
    lds tmp2, TIFR1        ; tmp <-- TIFR
    sbr tmp2, 1<<TOV1    ; Clear TOV1, write logic 1
    sts TIFR1, tmp2

    ; Start timer with new initial count
    sts TCNT1H,onHigh
    sts TCNT1L,onLow    ; load counter, read/write operations
    sts TCCR1B,tmp1        ; Restart timer

wait:
    lds tmp2,TIFR1     ; tmp <-- TIFR1
    sbrs tmp2,TOV1    ; check overflow flag
    rjmp wait
    ret

; ========================================
; Calculating frequency
; ========================================

getFrequency:
    ; Calculating frequency cycle: 1kHz -> 2kHz -> 4 kHz -> ...
    inc frequency     ; Adds 1 to frequency variable since button is pressed


    ldi tmp1,1
    cp frequency, tmp1
    breq freq_1k

    ldi tmp1, 2
    cp frequency, tmp1
    breq freq_2k

    ldi tmp1, 3
    cp frequency, tmp1
    breq freq_4k

    ldi tmp1, 4
    cp frequency, tmp1
    breq freq_loop
   
; ====================== NEEDS TO CHANGE FREQUENCY ACCORDINGLY ====

freq_1k:
    rcall LCD

    ldi r30, 0x88        ; load low bit
    ldi r31, 0xF0        ; load high bit

    sts TCNT1H, r31       
    sts TCNT1L, r30

    rjmp exit

freq_2k:
    rcall LCD
    rjmp exit

freq_4k:
    rcall LCD
    rjmp exit

freq_loop:
    ldi tmp1, 1
    mov frequency, tmp1
    rjmp freq_1k

exit:
    ret  

; ========================================
; Timer 0 overflow interrupt service routine
; ========================================
tim1_ovf:

    ; Check if pulled high or low
    sbis PINB, 0        ; Checking if low
    rjmp highSignal

    sbis PINB, 1        ; Checking if high
    rjmp lowSignal

highSignal:
    mov countHigh, onHigh
    mov countLow, onLow

lowSignal:
    mov countHigh, offHigh
    mov countLow, offLow

overflow:
        push r25        ; save r25 and sreg
        in r25, SREG
        push r25
        sbi PINC, 5        ; toggle PC5 by writing 1 to PINC, 5
        sts TCNT1H, countHigh
        sts TCNT1L, countLow
        pop r25            ; restore SREG and r25
        out sreg, r25
        pop r25

    reti

; ========================================
; getRPGDir:
; Determines clockwise or counterclockwise rotation
; ========================================
getRPGDir:
    mov newA, r0 ; moves a into c
    mov newB, r0 ; moves b into d

    ; reinitialize a and b registers to bit 0
    ;ldi a, 0
    ;ldi b, 0

    ; Checks signal in Channel A
    sbis PINB,0        ; if 0 then skip
    mov newA, temp        ; if 1 then load 1 into a

    ; Checks signal in Channel B
    sbis PINB,1        ; if 0 then skip
    mov newB, temp        ; if 1 then load 1 into b
       
    ; Compares for clockwise rotation
    cpse newA, oldB
    ;rcall incDutyCycle

    ; Compares for counter-clockwise rotation
    cpse newA, oldB
;    rcall decDutyCycle
       
    ; If neither compares work then no rotation (11)
    done:
        rcall LCD
        ret

; ========================================
; Incrementing duty cycle
; ========================================
/*
incDutyCycle:
   
    cpi frequency, 1
    breq incDC_1

    cpi frequency, 2
    breq incDc_2

    cpi frequency, 3
    breq incDC_4

incDC_1:
    adiw dcHigh:dcLow, 1
    adiw onHigh:onLow, 1
    sbiw offHigh:offLow, 1
    rjmp done

incDC_2:
    rjmp done

incDC_4:
    rjmp done

; ========================================
; Decrementing duty cycle
; ========================================

decDutyCycle:

    cpi frequency, 1
    breq decDC_1

    cpi frequency, 2
    breq decDC_2

    cpi frequency, 3
    breq decDC_4

decDC_1:
    sbiw dcHigh:dcLow, 1
    sbiw onHigh:onLow, 1
    adiw offHigh:offLow, 1
    rjmp done

decDC_2:
    rjmp done

decDC_4:
    rjmp done

*/
; DELAYS FOR INITIALIZING LCD
delay_5000us:
    mov savingTemp, tmp1

    ldi tmp1, 115
    mov delayVar, tmp1
d3:    mov delayVar1, tmp1
d4: dec delayVar1
    brne d4
    dec delayVar
    brne d3

    mov tmp1, savingTemp

    ret

delay_200us:
    mov savingTemp, tmp1

    ldi tmp1, 228
    mov delayVar, tmp1
d5:    dec delayVar
    nop
    nop
    nop
    nop
    brne d5

    mov tmp1, savingTemp

    ret

delay_100000us:
    mov savingTemp, tmp1

    ldi tmp1, 248
    mov delayVar, tmp1
d1:    mov delayVar1, tmp1
d2: dec delayVar1
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    brne d2
    dec delayVar
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    nop
    brne d1

    mov tmp1, savingTemp

    ret

; Initialize LCD screen: Set to 4-bit mode,
; proper cursor advance, clear screen
; no delay between nibbles
LCDInit:
    cbi PORTB, 5            ; Command mode: RS=0
                            ; Low to transfer commands

    sbi PORTB, 3            ; Setting Enable high

    ; delay .1 seconds
    rcall delay_100000us

    ; Write D7-4 = 3 hex
    ldi r23, 0x03
    out PORTC, r23
    rcall LCDStrobe

    ; Wait 5ms
    rcall delay_5000us

    ; Write D7-4 = 3 hex
    ldi r23, 0x03
    out PORTC, r23
    rcall LCDStrobe

    rcall delay_200us
   
    ; Write D7-4 = 3 hex
    ldi r23, 0x03
    out PORTC, r23
    rcall LCDStrobe

    ; Wait 200us
    rcall delay_200us
   
    ; Write D7-4 = 2 hex
    ldi r23, 0x02
    out PORTC, r23
    rcall LCDStrobe

    ; Wait 5ms
    rcall delay_5000us
   
    ; Write 28hex (4-bits, 2-lines)
    ldi r23, 0x02
    out PORTC, r23
    rcall LCDStrobe

    rcall delay_200us

    ldi r23, 0x08
    out PORTC, r23
    rcall LCDStrobe

    rcall delay_200us

    ; Write 08 hex (don't shift display, hide cursor)
    ldi r23, 0x00
    out PORTC, r23
    rcall LCDStrobe

    rcall delay_200us

    ldi r23, 0x08
    out PORTC, r23
    rcall LCDStrobe

    rcall delay_200us

    ; Write 01 hex (clear and home display)
    ldi r23, 0x00
    out PORTC, r23
    rcall LCDStrobe

    rcall delay_200us

    ldi r23, 0x01
    out PORTC, r23
    rcall LCDStrobe

    rcall delay_5000us

    ; Write 06 hex (move cursor right)
    ldi r23, 0x00
    out PORTC, r23
    rcall LCDStrobe

    rcall delay_200us

    ldi r23, 0x06
    out PORTC, r23
    rcall LCDStrobe

    rcall delay_200us

    ; Write 0C hex (turn on display)
    ldi r23, 0x00
    out PORTC, r23
    rcall LCDStrobe

    rcall delay_200us

    ldi r23, 0x0c
    out PORTC, r23
    rcall LCDStrobe

    rcall delay_200us

    ret

; LCDStrobe
LCDStrobe:
    ; PORTB, 3            ; Pull E low
    sbi PORTB, 3        ; Pull E (PB3) high
    nop
    nop
    cbi PORTB, 3        ; Pull E (PB3) low
    ret

delay_100us:
    mov savingTemp, tmp1

    ldi tmp1, 115
    mov delayVar, tmp1
d10:dec delayVar
    nop
    nop
    nop
    nop
    brne d10

    mov tmp1, savingTemp

    ret

; Display D string:
displayDString:
    ld r0, Z+
    tst r0                    ; reached end of message?
    breq done_dsd            ; Yes => quit
    swap r0                    ;
    out PORTC, r0
    rcall LCDStrobe
    swap r0
    out PORTC, r0
    rcall LCDStrobe
    rjmp displayDString
done_dsd:
    ret

 ; Display constant strings ("DC=" and "%")
 ; Send the string messages out, up to
 ; the terminating null byte
 displayCString:
    lpm
    swap r0
    out PORTC, r0
    rcall LCDStrobe
    rcall delay_100us
    swap r0
    out PORTC, r0
    rcall LCDStrobe
    rcall delay_100us
    adiw zh:zl, 1
    dec r21
    brne displayCString
    ret

; Code to create a string into RAM
displayDC:

    ; Copy number we want to display from R25 and R26
    ; to 16-bit unsigned divide routine uses
    mov dd16uL, r25        ; LSB of number to display
    mov dd16uH, r26        ; MSB of number to display

    ; Divide by high and low bit by 10
    ldi dv16uL, low(10)
    ldi dv16uH, high(10)

    ; Store terminating for the string
    ldi r20, 0x00        ; Terminating NULL
    sts dtxt+4, r20        ; Store in RAM (back of the string)

    ; Divide the number by 10 and format remainder
    rcall div16u        ; Result: r17 and r15, remember r15 and r14
    ldi r20, 0x30        ; Add 0x30 to remainder to convert to ASCII code
    add r14, r20        ; Convert to ASCII, r14 stores remainder
    sts dtxt+3, r14        ; Store in RAM (back of the string)

    ; Generate decimal point.
    ldi r20, 0x2e        ; ASCII code for decimal point "."
    sts dtxt+2, r20        ; Store in RAM (back of the string)

    ; Divide the number by 10 and format remainder
    rcall div16u       
    ldi r20, 0x30        ; Stores 0x30 into r20
    add r14, r20        ;
    sts dtxt+1, r14        ; Store in RAM (back of the string)

    ; Add remainder to the end
    rcall div16u
    ldi r20, 0x30       
    add r14, r20       
    sts dtxt, r14        ; Store in RAM (back of the string)

    ; Store the isolated duty cycle into registers
    ldi r30, low(dtxt)
    ldi r31, high(dtxt)
    rcall displayDString    ; Display duty cycle

    ret

; Div16u: 16/16 bit unsigned division. Divides the two 16-bit numbers.
; dd8uH:dd8uL dividend and dv16uH:dv16uL divisor
; result placed in dres16uH:dres16uL and remainder in drem16uH:drem16uL
div16u:
        clr    drem16uL            ; clear remainder Low byte
        sub    drem16uH,drem16uH    ; clear remainder High byte and carry
        ldi    dcnt16u,17            ; loop counter
d16u_1:    rol    dd16uL                ; shift left dividend
        rol    dd16uH
        dec    dcnt16u                ; decrement counter
        brne d16u_2                ; if done
        ret                        ; return
d16u_2:    rol    drem16uL            ; shift dividend into remainder
        rol    drem16uH
        sub    drem16uL,dv16uL        ; remainder = remainder - divisor
        sbc    drem16uH,dv16uH        ;
        brcc d16u_3                ; if result negative
        add    drem16uL,dv16uL        ; restore remainder
        adc    drem16uH,dv16uH
        clc                        ; clear carry to be shifted into result
        rjmp    d16u_1            ; else
d16u_3:    sec                        ; set carry to be shifted into result
        rjmp    d16u_1

    /*
    ; Enable overflow interrupt on 8-bit timer 0
    ; TMSK1 is outside the 0...63 address
    lds r24, TIMSK1        ; grab 8-bit timer's interrupt mask
    ori r24, 0x01        ; Overflow interrupt enable
    sts TIMSK1, r24        ; update interrupt mask

    ; Configuring timer
    ; turn timer 0 on, use system clock, prescaled
    ; with 256 => increment at 10MHz / 256 => 25.6 us/tick
    ldi r24, 0x04
    out TCCR0B, r24
    */

    /*
    ; setting duty cycle to 50% ??
    mov savingTemp, tmp1

    ldi tmp1, 102                                                                                       
    mov dutycycle, tmp1

    mov tmp1, savingTemp
    */

.exit