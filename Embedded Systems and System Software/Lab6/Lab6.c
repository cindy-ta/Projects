//
//  Lab6.c
//  Embedded Systems
//
//  Cindy Ta
//  Phil Partipilo
//
//


#include <stdio.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/pgmspace.h>

#include "i2cmaster.h"

#define F_CPU 8000000UL
#define BAUDRATE 9600

void USART_init();
void USART_print(const char *);
char USART_get(void);
void USART_put(char);
void ADC_init(void);
void readADC(void);

const uint8_t sine_table[64]  = {128,141,153,165,177,188,199,209,219,227,234,241,246,250,254,255,255,255,254,250,246,241,234,227,219,209,199,188,177,165,153,141,128,115,103,91,79,68,57,47,37,29,22,15,10,6,2,1,0,1,2,6,10,15,22,29,37,47,57,68,79,91,103,115};

float v;
char userInput=0;
char userRead[25];
char userWrite[25];
int integer;
int floatpart;
int i;


int main(void) {
    // initializes USART, ADC and i2c
    USART_init();
    ADC_init();
    i2c_init();
    
    int check1;
    int check2;
    int check3;
    uint8_t j;
    char *ptr;
    
    while(1) {
        // read from USART
        // echo
        uint8_t counter=0;
        while(userInput != '\r') // stops reading after user presses enter
        {
            userInput = USART_get();
            
            USART_put(userInput);
            userRead[counter]=userInput;
            
            counter++;
        }
        
        check1 = 0;
        check2 = 0;
        check3 = 0;
        
        // #### G command
        if (userRead[0] == 'G') {
            readADC();
            integer = voltage;
            voltage = voltage - integer;
            voltage = voltage * 1000;
            floatpart = voltage;
            sprintf(userWrite,"\nv=%d.%d V\n\n\r",integer, floatpart);
            USART_print(userWrite);
        }
        
        // #### M command
        else if (userRead[0] == 'M') {
            
            clearWrite();
            
            for(j=2;userRead[j]!=','&&j<24;j++) {
                if ((userRead[j]-0x30)>=0&&(userRead[j]-0x30)<=9) {
                    *ptr=userRead[j];
                    ptr = ptr + 1;
                }
            }
            
            j++;
            check1 = atoi(&userWrite);
            
            clearWrite();
            
            for(; j < 24; j++) {
                if ((userRead[j] - 0x30) >= 0 && (userRead[j] - 0x30) <= 9) {
                    *ptr=userRead[j];
                    ptr = ptr + 1;
                }
            }
            
            check2 = atoi (&userWrite);
            
            // execution
            if(check1 >= 2 && check1 <= 20 && check2 >= 1 && check2 <= 60) {
                    
                for(j=0; j <= (check1 - 1); j++) {
                    readADC();
                    integer = voltage;
                    voltage = voltage - integer;
                    voltage = voltage * 1000;
                    floatpart = voltage;
                    sprintf(userWrite,"\nt=%d s, v=%d.%d V\r",(check2*j), integer, floatpart);
                    USART_print(userWrite);
                    
                    for(i=0; i<check2; i++) {
                        _delay_ms(1000);
                    }
                }
            }

            USART_print("\n\n");
        }
        
        // S command
        else if (userRead[0] == 'S') {
            
            clearWrite();
            
            for (j = 2; j < 24 && userRead[j] != ','; j++) {
                if ((userRead[j] - 0x31) >= 0 && (userRead[j] - 0x31) <= 9) {
                    *ptr = userRead[j];
                    ptr = ptr + 1;
                }
            }
            
            j++;
            check1 = atoi(&userWrite);
            
            clearWrite();
                
            for(;j < 24 && userRead[j] != '.'; j++) {
                if ((userRead[j] - 0x30) >= 0 && (userRead[j] - 0x30) <= 9) {
                    *ptr = userRead[j];
                    ptr = ptr + 1;
                }
            }
            
            j++;
            
            check2 = atoi(&userWrite);
           
            clearWrite();
        
            for(;j < 24; j++) {
                if ((userRead[j] - 0x30) >= 0 && (userRead[j] - 0x30) <= 9 && check3<=2) {
                    *ptr = userRead[j];
                    ptr = ptr + 1;
                    check3 = check3 + 1;
                }
            }
            
            j = check3;
            check3 = (atoi(&userWrite));
            
            for(i = (2-j); i >= 0; i--) {
                check3 = check3 * 10;
            }
            
            // execute S
            sprintf(userWrite, "\nDAC channel %d set to %d.", check1, check2);
            USART_print(userWrite);
            USART_put(check3/100+0x30);
            USART_put((check3%100)/10+0x30);
            USART_print(" V\n\n\r");
            
            if(check1 <= 1 && check2 >= 0 && check2 <= 5 && check3 >= 0 && check3 < 1000) {
                j= 51 * check2 + check3 / 19;
                
                if(check1 == 0) {
                    i2c_start(0b01011000);
                    i2c_write(0x00);
                    i2c_write(j);
                    i2c_stop();
                }
                
                if(check1 == 1) {
                        i2c_start(0b01011000);
                        i2c_write(0x01);
                        i2c_write(j);
                        i2c_stop();
                    }
                    
                }
            }
        }
    
        // ##### Command W
        else if (userRead[0] == 'W') {
            clearWrite();
            
            for(j = 2; j < 24 && userRead[j] != ','; j++) {
                if ((userRead[j] - 0x30) >= 0 && (userRead[j] - 0x30) <= 9 ) {
                    *ptr=userRead[j];
                    ptr = ptr + 1;
                }
            }
            
            j++;
            check1 = atoi(&userWrite);
            
            for(i=0;i<24;i++) {
                userWrite[i]=0;
            }
            ptr=&userWrite[0];
            
            for(; j < 24 && userRead[j] != ','; j++)        //parse argument 2
            {
                if ((userRead[j] - 0x30) >= 0 && (userRead[j] - 0x30) <= 9) {
                    *ptr=userRead[j];
                    ptr = ptr + 1;
                }
            }
            
            j++;
            check2 = atoi(&userWrite);
            
            for(i=0; i < 24; i++) {
                userWrite[i]=0;
            }
            
            ptr=&userWrite[0];
            
            for(; j < 24; j++) {
                if ((userRead[j] - 0x30) >= 0 && (userRead[j] - 0x30) <= 9 && check3 <= 2) {
                    *ptr = userRead[j];
                    ptr = ptr + 1;
                }
            }
            j = check3;
            check3 = atoi(&userWrite);
            
            //execute
            if ((check2 == 5 || check2 == 10 || check2 == 15 || check2 == 20 || check2 == 25) && check1 <= 1 && check3 >= 1 && check3 <= 1000){
                sprintf(userWrite,"\n\rgenerating %d sine wave cycles with f=%d Hz on DAC channel %d\n\r",check3,check,check1);
                USART_print(userWrite);
                j = 100 / check2;
                for(int k=0; k < check3; k++)
                {
                    for(i=0; i < 63; i++)
                    {
                        
                        i2c_start(0b01011000);
                        i2c_write(check1);
                        i2c_write(sine_table[i]);
                        i2c_stop();
                        
                        // frequency = 5
                        if (check2 == 5) {
                            _delay_us(3080);
                        }
                        
                        // frequency = 10
                        else if (check2 == 10) {
                            _delay_us(1455);
                        }
                        
                        // frequency = 15
                        else if (check2 == 15) {
                            _delay_us(920);
                        }
                        
                        // frequency = 20
                        else if (check2 == 20) {
                            _delay_us(648);
                        }
                        
                        // frequency = 25
                        else if (check2 == 25) {
                            _delay_us(488);
                        }
                    }
                }
            }
            
            USART_print("\n");
        }
        
        userInput = 0;
            
        for(i=0; i < 25; i++) {
            userRead[i]=0;
        }
    }
}

// USART_init
// initializes uart
void USART_init() {
    UBRR0 = BAUDRATE;//set baud rate
    UCSR0A = (0<<U2X0);//normal mode
    UCSR0C=0x86; //set 8N1
    UCSR0B=(1<<RXEN0)|(1<<TXEN0); //enable transmit/receive
    return;
}

// USART_print
// Prints to USART
void USART_print(const char *ptr) {
    while(*ptr) {
        while ( !(UCSR0A & (1 << UDRE0)))
            ;
        UDR0 = *(ptr = ptr + 1);
    }
    return;
}

// USART_get
// gets character entered by user
char USART_get(void) {
    while ( !(UCSR0A & (1 << RXC0)));
    
    return UDR0;
}

// USART_put
// prints character onto uart
void USART_put(char c) {
    while ( !(UCSR0A & (1 << UDRE0)));
    
    UDR0 = c;
    return;
}

// ADC_init
// initializes ADC for use
// using PC0/ADC0 for data
void ADC_init(void) {
    
    // prescaler: 8Mhz/64 = 125Khz
    ADCSRA = ADCSRA | (1 << ADPS2);
    ADCSRA = ADCSRA | (1 << ADPS1);
    ADCSRA = ADCSRA | (0 << ADPS0);
    
    // Voltage reference from A Vcc (5V)
    ADMUX = ADMUX | (1<<REFS0);
    
    // using PC0
    ADMUX = ADMUX & 0xF0;
    
    // Turn on ADC
    ADCSRA = ADCSRA | (1<<ADEN);
    
    // initial conversion
    ADCSRA ADCSRA | (1<<ADSC);
    
    return;
}

// readADC
// read in analog value and converts to digital
void readADC(void) {
    
    // new conversion
    ADCSRA = ADCSRA | (1 << ADSC);
    while (ADCSRA & (1 << ADSC));       // wait until the conversion is done
    
    voltage = ADC;
    voltage = (voltage * 5) / 1024;
}

// clearWrite
// clears userWrite array
void clearWrite() {
    for(i = 0; i < 24; i++) {
        userWrite[i]=0;
    }
    
    ptr = &userWrite[0];
}