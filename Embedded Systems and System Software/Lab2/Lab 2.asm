/*
 * Lab_2.asm
 *  Programmers: Phil Partipilo, Cindy Ta
 *  Created: 2/3/2015 10:30:47 AM
 */ 

.include "tn45def.inc"
.cseg
.org 0

; Configure PB1 as output pin
	sbi DDRB,1		; PB1 is now output
	cbi DDRB,0		; PB0 is an input

	sbi PORTB,1		; turn LED off

; MAIN
	main:
		; CHECKS SWITCH BUTTON
		sbic PINB,0  ; checks if low
		rjmp main	; if button is not pressed, reloops through main
		rjmp sequence	; if button pressed, go through sequence
		end_if:
			rjmp main
	 
; one: returns bit "1"
	one:
		rcall bit_889delay	; delays for 8840 cycles

		ldi r31, 33 	; loops 33 times
		d11:
		sbi PORTB,1 ; LED off

		rcall bit_694delay	; delay 63 cycles

		cbi PORTB,1 ; LED on

		rcall bit_2080delay		; delays for 203 cycles
		
		dec r31
		brne d11

		ret

; zero: returns bit "0"
	zero:
		ldi r30, 33		; loops 33 times
		d10:
		cbi PORTB,1 ; LED on
		
		rcall bit_694delay	; delays for 63 cycles

		sbi PORTB,1 ; LED off

		rcall bit_2080delay		; delays for 203 cycles

		dec r30
		brne d10

		rcall bit_889delay	; delays for 8840 cycles
		ret

; Sequence
	sequence:
		; S1:
		rcall one
		
		; S2:
		rcall one

		; T:
		rcall zero

		; VCR2:
		rcall zero
		rcall zero
		rcall one
		rcall one
		rcall zero

		; PLAY:
		rcall one
		rcall one
		rcall zero
		rcall one
		rcall zero
		rcall one

		rcall delay_29ms ; delay of 289K cycles

		; S1:
		rcall one

		; S2:
		rcall one

		; T:
		rcall one

		; TV2:
		rcall zero
		rcall zero
		rcall zero
		rcall zero
		rcall one

		; VOLUME ++:
		rcall zero
		rcall one
		rcall zero
		rcall zero
		rcall zero
		rcall zero

		rjmp main

; delay 29ms: delays for 29.008ms 
	delay_29ms: ; 289K cycles
		ldi r23, 42 ; sets 42 bits into register 23
	d1: ldi r24, 51 ; sets 51 bits into register 24
	d2: ldi r25, 43 ; sets 43 bits into register 25
	d3: dec r25 ; decrements register 25
		brne d3 ; checks to see if register 25 is zero
		dec r24 ; decrements register 24
		nop ; no operation
		nop
		nop
		brne d2 ; checks to see if register 24 is zero
		dec r23 ; decrements register 23
		brne d1 ; checks to see if register 23 is zero
		ret

; bit delay of 889 microseconds
	bit_889delay: ; 8840-5 cycles
		ldi r20, 13 ; sets 13 bits into register 20
	d4: ldi r21, 13 ; sets 13 bits into register 21
	d5: ldi r22, 12 ; sets 12 bits into register 22
	d6: dec r22
		nop
		brne d6
		dec r21
		nop
		brne d5
		dec r20
		brne d4
		ret
		

; bit delay of 6.94 microseconds
	bit_694delay:	; 71-5 cycles
		ldi r17, 2 ; sets 2 bits into register 17
	d7: ldi r18, 2 ; sets 2 bits into register 18
	d8: ldi r19, 2 ; sets 2 bits into register 19
	d9: dec r19
		nop
		nop
		brne d9
		dec r18
		brne d8
		dec r17
		brne d7
		ret

; bit delay of 208 microseconds
	bit_2080delay:  ; 203-5 cycles
		ldi r26, 2 ; sets 2 bits into register 26
	d20: ldi r27, 4 ; sets 4 bits into register 27
	d21: ldi r28, 5 ; sets 5 bits into register 28
	d22: dec r28
		nop
		brne d22
		dec r27
		brne d21
		dec r26
		brne d20
		ret

.exit